# OpenML dataset: GDP-per-capita-all-countries

https://www.openml.org/d/43809

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Gross Domestic Product (GDP) is the monetary value of all finished goods and services made within a country during a specific period. GDP provides an economic snapshot of a country, used to estimate the size of an economy and growth rate.
This dataset contains the GDP based on Purchasing Power Parity (PPP). 
GDP comparisons using PPP are arguably more useful than those using nominal GDP when assessing a nation's domestic market because PPP takes into account the relative cost of local goods, services and inflation rates of the country, rather than using international market exchange rates which may distort the real differences in per capita income
Acknowledgement
Thanks to World Databank

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43809) of an [OpenML dataset](https://www.openml.org/d/43809). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43809/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43809/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43809/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

